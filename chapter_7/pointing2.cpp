#include <iostream>
#include <string>

using namespace std;

/*

Constant pointer
- int* const pScore = &score
- can only point to the object that it was first initalized to
- must be initialzed to something
- ILLEGAL: pScore = &anotherScore
- LEGAL: *pScore = 500

Pointer to a constant
- const int* pNumber
- does not need to be initialized in beginning
- int lives = 3;
- pNumber = &lives
- Cannot dereference to change the value it points to
- ILLEGAL: *pNumber = -1 
- const int MAX_LIVES = 5;
- pNumber = &MAX_LIVES; //pointer itself can change

Constant pointer to a constant
- Kill me
- const int* const pBonus = &BONUS; //Constant pointer to a constant
- can point to a non-constant or a constant value
- can't be reassigned
- ILLEGAL pBONUS = &MAX_LIVES; can't point to another object
- ILLEGAL *pBONUS = MAX_LIVES can change value of dereferenced pointer

You can restrict a poiter so that it only points to an object it
was initialized to
or restrict it sot hat it can't change the value it references

or both

*/

int main(){
  int score = 100;
  int* const pScore = &score; //Constant Pointer

  cout << *pScore << "\n";
  *pScore = 500;
  cout << "new *pScore: " << *pScore << "\n";

  const int* pNumber; //Pointer to a constant
  int lives = 3;
  pNumber = &lives;
  
  return 0;
}
