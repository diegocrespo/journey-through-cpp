// Overloading and default arguments
#include <iostream>
#include <string>

using namespace std;

string askNumber(string prompt="Give me a number ");

int main()
{
  string number;
  number = askNumber();
  cout << "You number was " << number << endl;
  string newNumber = askNumber("Give me another number");
  cout << "Your New number was " << newNumber << endl;
}

string askNumber(string prompt)
{
  string number;
  cout << prompt;
  cin >> number;
  return number;
}
