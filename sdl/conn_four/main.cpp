//Same as above just wrapped in functions
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <cmath>


const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

bool init();

void close();

SDL_Window* g_window = nullptr;

SDL_Surface* g_screen_surface = nullptr;

//When dealing with textures you need sdl_renderer
//To render them on the screen
SDL_Renderer* g_renderer = nullptr;

//Arguements are an interger followed by char* array
//SDL requires this to be compatible with other platforms
int main( int argc, char* args[] ){


  if( !init() ){
    printf("Failed to initiliaze SDL!\n");
  }
  else{
    //Main loop flag
    bool quit = false;

    //Event handler
    SDL_Event e;

    //While application is running
    while( !quit ){
      while( SDL_PollEvent( &e ) != 0){
	//User requests quit
	if( e.type == SDL_QUIT ){
	  quit = true;
	}
      }
      //Clear screen
      SDL_SetRenderDrawColor( g_renderer, 0xFF, 0xFF, 0xFF, 0xFF );
      SDL_RenderClear( g_renderer);
      //Render red filled quad
      int tile_width = SCREEN_WIDTH / 8 - 10;
      int tile_height = SCREEN_HEIGHT / 8 - 10;

      int xspacer = 10;      	

      for( int row = 0; row < 8; ++row){
	int yspacer = 10;
	for( int col= 0; col < 8; ++col){
	  SDL_SetRenderDrawColor( g_renderer, 0xFF, 0x00, 0x00, 0xFF );	  
	  SDL_Rect fillRect = { xspacer, yspacer, tile_width , tile_height };
	  SDL_RenderFillRect( g_renderer, &fillRect );

	  yspacer = yspacer + 58;
	}
	  xspacer = xspacer + 80;	
	
      }

      SDL_RenderPresent( g_renderer );
    }
  }
  //Destroy window
  close();
  return 0;  
}

bool init (){

  //Initialization flag
  bool success = true;
    //Initialize SDL
  if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){
    printf("SDL could not be initialized! SDL_Error:  %s\n", SDL_GetError() );
    return success = false;
  }
  else{
    //Create window
    g_window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );

    if( g_window == nullptr ){
      printf("Window could not be created! SDL_Error:  %s\n", SDL_GetError() );
      return success = false;
    }
    else{
      g_renderer = SDL_CreateRenderer( g_window, -1, SDL_RENDERER_ACCELERATED );
      if( g_renderer == nullptr ){
	printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
	success = false;
      }
      else{
	SDL_SetRenderDrawColor( g_renderer, 0xFF, 0xFF, 0xFF, 0xFF );
      }
    }
  }
  return success;	  
}

void close(){
  SDL_DestroyRenderer( g_renderer );
  SDL_DestroyWindow( g_window );
  g_window = nullptr;
  g_renderer = nullptr;
  
  //Quit SDL subsystems
  SDL_Quit();
  
}
