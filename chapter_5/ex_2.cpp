// Hangman
// Hangman with functions

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cctype>

using namespace std;

char getGuess(string prompt, string used);

string isSecret(char guess, string secret, string soFar);

const int MAX_WRONG = 8;
int wrong = 0;
string used = "";

int main()
{
  cout << "Welcome to Hangman. Good luck!\n";

  vector<string> words; //collection of possible words to guess
  words.push_back("GUESS");
  words.push_back("HANGMAN");
  words.push_back("DIFFICULT");
  
  srand(static_cast<unsigned int>(time(0)));
  random_shuffle(words.begin(), words.end());
  const string THE_WORD = words[0];
  string soFar(THE_WORD.size(), '-'); //word guessed so far

  //main Loop
  while ((wrong < MAX_WRONG) && (soFar != THE_WORD))
    {
      cout << "\n\nYou have " << (MAX_WRONG - wrong);
      cout << " incorrect guesses left.\n";
      cout << "\nYou've used the following letters:\n" << used << endl;
      cout << "\nSo far, the word is:\n" << soFar << endl;

      char guess = getGuess("\n\nEnter your guess: ", used);
      used += guess;
      soFar = isSecret(guess, THE_WORD, soFar);
    }

  if (wrong == MAX_WRONG)
    {
      cout << "\nYou've been hanged!" << endl;
      cout << "The word was " << THE_WORD << endl;
      
    }
  else
    {
      cout << "\nYou guessed it!" << endl;
      cout << "The word was " << THE_WORD << endl;
    }
  return 0;  
}

char getGuess(string prompt, string used)
{
  char guess;
  cout << prompt;
  cin >> guess;
  guess = toupper(guess); //make uppercase since secret word in uppercase
  while (used.find(guess) != string::npos)
    {
      cout << "\nYou've already guess " << guess << endl;
      cout << "Enter your guess: ";
      cin >> guess;
      guess = toupper(guess);
      }

  return guess;
}

string isSecret(char guess, string THE_WORD, string soFar)
{
  if (THE_WORD.find(guess) != string::npos)
    {
      cout << "That's right! " << guess << " is in the word.\n";
      //update soFar to include newly guess letter
      for (int i = 0; i < THE_WORD.length(); ++i)
	{
	  if (THE_WORD[i] == guess)
	    {
	      soFar[i] = guess;
	    }
	}
    }
  else
    {
      cout << "Sorry, " << guess << " isn't in the word.\n";
      wrong = wrong + 1;
    }
  return soFar;
}
