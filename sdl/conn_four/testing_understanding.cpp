//The most basic way to get something onto screen in sdl

// #include <iostream>
// #include <SDL2/SDL.h>
// #include <stdio.h>

// const int SCREEN_WIDTH = 640;
// const int SCREEN_HEIGHT = 480;

// //Arguements are an interger followed by char* array
// //SDL requires this to be compatible with other platforms
// int main( int argc, char* args[] ){

//   //The window we'll be rendering to
//   SDL_Window* Window = nullptr;

//   //The surface contained by the window
//   SDL_Surface* screen_surface = nullptr;

//   //Initialize SDL
//   if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){
//     printf("SDL could not be initialized! SDL_Error:  %s\n", SDL_GetError() );
//   }
//   else{
//     //Create window
//     Window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
//     if( Window == nullptr ){
//       printf("Window could not be created! SDL_Error:  %s\n", SDL_GetError() );
//     }
//     else{
//       //Get window surface
//       screen_surface = SDL_GetWindowSurface( Window );

//       //Fill the surfacew blue
//       SDL_FillRect( screen_surface, NULL, SDL_MapRGB( screen_surface->format, 0xFF, 0xFF, 0xFF ) );

//       //Update the surface
//       SDL_UpdateWindowSurface( Window );

//       //Wait two seconds
//       SDL_Delay( 2000 );

//     }
//   }
//   //Destroy window
//   SDL_DestroyWindow( Window );

//   //Quit SDL subsystems
//   SDL_Quit();
//   return 0;
// }

//Same as above just wrapped in functions
#include <SDL2/SDL.h>
#include <stdio.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

bool init();

void close();

SDL_Window* g_window = nullptr;

SDL_Surface* g_screen_surface = nullptr;

//Arguements are an interger followed by char* array
//SDL requires this to be compatible with other platforms
int main( int argc, char* args[] ){


  if( !init() ){
    printf("Failed to initiliaze SDL!\n");
  }
  else{
    //Get window surface
    g_screen_surface = SDL_GetWindowSurface( g_window );
    //Fill the surfacew blue
    SDL_FillRect( g_screen_surface, NULL, SDL_MapRGB( g_screen_surface->format, 0xFF, 0xFF, 0xFF ) );

    //Update the surface
    SDL_UpdateWindowSurface( g_window );

    //Wait two seconds
    SDL_Delay( 2000 );
  }
  //Destroy window
  close();
  return 0;
}

bool init (){

  //Initialization flag
  bool success = true;
    //Initialize SDL
  if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){
    printf("SDL could not be initialized! SDL_Error:  %s\n", SDL_GetError() );
    return success = false;
  }
  else{
    //Create window
    g_window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
    
    if( g_window == nullptr ){
      printf("Window could not be created! SDL_Error:  %s\n", SDL_GetError() );
      return success = false;
    }
    else{
      return success;
    }
  }
}

void close(){
  
  SDL_DestroyWindow( g_window );
  g_window = nullptr;
  
  //Quit SDL subsystems
  SDL_Quit();
  
}
