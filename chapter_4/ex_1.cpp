// Favorite games list

#include <iostream>
#include <vector>
#include <string>

using namespace std;


// int main()
// {
//   vector<string> gameList;
//   string guess;
//   vector<string>::const_iterator iter;
//   bool quit = false

//   while ((cin != string::npos) || (cin != "QUIT"))
//     {
//       cout << "add another game?" << endl;
//       cin >> guess;
//       gameList.push_back(guess);
//     }
//   for (iter = gameList.begin(); iter != gameList.end(); ++iter)
//     {
//       cout << *iter << endl;
//     }
//   return 0;
// }


int main()
{
  vector<string> gameList;
  string answer;
  vector<string>::const_iterator iter;
  bool quit = false;
  while (quit != true)
    {
      cout << "\n\nEnter your answer: ";
      cin >> answer;
      gameList.push_back(answer);

      while (answer != "QUIT")
	{
	  cout << "add game" << endl;
	  cin >> answer;
	  gameList.push_back(answer);
	}
      quit = true;
    }

  for (iter = gameList.begin(); iter != gameList.end(); ++iter)
    {
      cout << *iter << endl;
    }
  return 0;
}
